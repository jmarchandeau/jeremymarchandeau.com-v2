/*
 * ATTENTION: The "eval" devtool has been used (maybe by default in mode: "development").
 * This devtool is neither made for production nor for readable output files.
 * It uses "eval()" calls to create a separate source file in the browser devtools.
 * If you are trying to read the output file, select a different devtool (https://webpack.js.org/configuration/devtool/)
 * or disable the default devtool with "devtool: false".
 * If you are looking for production-ready output files, see mode: "production" (https://webpack.js.org/configuration/mode/).
 */
/******/ (() => { // webpackBootstrap
/******/ 	"use strict";
/******/ 	var __webpack_modules__ = ({

/***/ "./assets/src/js/_generalScripts.js":
/*!******************************************!*\
  !*** ./assets/src/js/_generalScripts.js ***!
  \******************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"default\": () => (__WEBPACK_DEFAULT_EXPORT__)\n/* harmony export */ });\n/* harmony import */ var _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/helpers/classCallCheck */ \"./node_modules/@babel/runtime/helpers/esm/classCallCheck.js\");\n/* harmony import */ var _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @babel/runtime/helpers/createClass */ \"./node_modules/@babel/runtime/helpers/esm/createClass.js\");\n\n\nvar General = /*#__PURE__*/function () {\n  function General() {\n    (0,_babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_0__[\"default\"])(this, General);\n    this.testVariable = 'script working';\n    this.init();\n  }\n  (0,_babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_1__[\"default\"])(General, [{\n    key: \"init\",\n    value: function init() {\n      // for tests purposes only\n      console.log(this.testVariable);\n    }\n  }]);\n  return General;\n}();\n/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (General);\n\n//# sourceURL=webpack://blank-theme/./assets/src/js/_generalScripts.js?");

/***/ }),

/***/ "./assets/src/js/main.js":
/*!*******************************!*\
  !*** ./assets/src/js/main.js ***!
  \*******************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _generalScripts__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./_generalScripts */ \"./assets/src/js/_generalScripts.js\");\n\nvar App = {\n  /**\n   * App.init\n   */\n  init: function init() {\n    // General scripts\n    function initGeneral() {\n      return new _generalScripts__WEBPACK_IMPORTED_MODULE_0__[\"default\"]();\n    }\n    initGeneral();\n  }\n};\ndocument.addEventListener('DOMContentLoaded', function () {\n  App.init();\n});\n\n/**\n * Adds an 'is-active' class to the first list item in the 'services-list__item' class\n * when the window has finished loading.\n *\n * @function\n * @name addActiveClassToFirstListItem\n * @returns {void}\n */\n\nwindow.addEventListener('load', function () {\n  var firstListItem = document.querySelector('li.services-list__item');\n  firstListItem.classList.add('is-active');\n});\n\n/**\n * Adds event listeners to show and hide a contact popup when certain elements are clicked or keys are pressed.\n * @function\n * @name addContactPopupListeners\n * @param {Event} DOMContentLoaded - The DOMContentLoaded event.\n * @returns {void}\n */\ndocument.addEventListener('DOMContentLoaded', function () {\n  // Select relevant DOM elements\n  var contactMeBtns = document.querySelectorAll('.contact-me');\n  var contactPopup = document.querySelector('#contact-popup');\n  var closeBtns = document.querySelectorAll('.close');\n  var popup = document.querySelector('.popup');\n\n  // Show popup when any of the '.contact-me' buttons are clicked\n  contactMeBtns.forEach(function (contactMeBtn) {\n    contactMeBtn.addEventListener('click', function () {\n      contactPopup.style.display = 'block';\n    });\n  });\n\n  // Hide popup when the close button or outside the popup is clicked\n  closeBtns.forEach(function (closeBtn) {\n    closeBtn.addEventListener('click', function () {\n      contactPopup.style.display = 'none';\n    });\n  });\n\n  // Hide popup when clicking outside of it\n  popup.addEventListener('click', function (e) {\n    if (e.target === popup) {\n      contactPopup.style.display = 'none';\n    }\n  });\n\n  // Hide popup when the escape key is pressed\n  document.addEventListener('keyup', function (e) {\n    if (e.keyCode === 27) {\n      contactPopup.style.display = 'none';\n    }\n  });\n});\n\n/* ===================================================================\n * Ethos 1.0.0 - Main JS\n *\n * ------------------------------------------------------------------- */\n\n(function ($) {\n  \"use strict\";\n\n  var cfg = {\n    scrollDuration: 800,\n    // smoothscroll duration\n    mailChimpURL: '' // mailchimp url\n  };\n\n  var $WIN = $(window);\n\n  // Add the User Agent to the <html>\n  // will be used for IE10/IE11 detection (Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.2; Trident/6.0; rv:11.0))\n  var doc = document.documentElement;\n  doc.setAttribute('data-useragent', navigator.userAgent);\n\n  /* pretty print\n   * -------------------------------------------------- */\n  var ssPrettyPrint = function ssPrettyPrint() {\n    $('pre').addClass('prettyprint');\n    $(document).ready(function () {\n      prettyPrint();\n    });\n  };\n\n  /* move header\n   * -------------------------------------------------- */\n  var ssMoveHeader = function ssMoveHeader() {\n    var $hero = $('.s-hero'),\n      $hdr = $('.s-header'),\n      triggerHeight = $hero.outerHeight() - 170;\n    $WIN.on('scroll', function () {\n      var loc = $WIN.scrollTop();\n      if (loc > triggerHeight) {\n        $hdr.addClass('sticky');\n      } else {\n        $hdr.removeClass('sticky');\n      }\n      if (loc > triggerHeight + 20) {\n        $hdr.addClass('offset');\n      } else {\n        $hdr.removeClass('offset');\n      }\n      if (loc > triggerHeight + 150) {\n        $hdr.addClass('scrolling');\n      } else {\n        $hdr.removeClass('scrolling');\n      }\n    });\n  };\n\n  /* mobile menu\n   * ---------------------------------------------------- */\n  var ssMobileMenu = function ssMobileMenu() {\n    var $toggleButton = $('.header-menu-toggle');\n    var $headerContent = $('.header-content');\n    var $siteBody = $(\"body\");\n    $toggleButton.on('click', function (event) {\n      event.preventDefault();\n      $toggleButton.toggleClass('is-clicked');\n      $siteBody.toggleClass('menu-is-open');\n    });\n    $headerContent.find('.header-nav a, .btn').on(\"click\", function () {\n      // at 900px and below\n      if (window.matchMedia('(max-width: 900px)').matches) {\n        $toggleButton.toggleClass('is-clicked');\n        $siteBody.toggleClass('menu-is-open');\n      }\n    });\n    $WIN.on('resize', function () {\n      // above 900px\n      if (window.matchMedia('(min-width: 901px)').matches) {\n        if ($siteBody.hasClass(\"menu-is-open\")) $siteBody.removeClass(\"menu-is-open\");\n        if ($toggleButton.hasClass(\"is-clicked\")) $toggleButton.removeClass(\"is-clicked\");\n      }\n    });\n  };\n\n  /* accordion\n   * ------------------------------------------------------ */\n  var ssAccordion = function ssAccordion() {\n    var $allItems = $('.services-list__item');\n    var $allPanels = $allItems.children('.services-list__item-body');\n    $allPanels.slice(1).hide();\n    $allItems.on('click', '.services-list__item-header', function () {\n      var $this = $(this),\n        $curItem = $this.parent(),\n        $curPanel = $this.next();\n      if (!$curItem.hasClass('is-active')) {\n        $allPanels.slideUp();\n        $curPanel.slideDown();\n        $allItems.removeClass('is-active');\n        $curItem.addClass('is-active');\n      }\n      return false;\n    });\n  };\n\n  /* photoswipe\n   * ----------------------------------------------------- */\n  var ssPhotoswipe = function ssPhotoswipe() {\n    var items = [],\n      $pswp = $('.pswp')[0],\n      $folioItems = $('.folio-item');\n\n    // get items\n    $folioItems.each(function (i) {\n      var $folio = $(this),\n        $thumbLink = $folio.find('.folio-item__thumb-link'),\n        $title = $folio.find('.folio-item__title'),\n        $caption = $folio.find('.folio-item__caption'),\n        $titleText = '<h4>' + $.trim($title.html()) + '</h4>',\n        $captionText = $.trim($caption.html()),\n        $href = $thumbLink.attr('href'),\n        $size = $thumbLink.data('size').split('x'),\n        $width = $size[0],\n        $height = $size[1];\n      var item = {\n        src: $href,\n        w: $width,\n        h: $height\n      };\n      if ($caption.length > 0) {\n        item.title = $.trim($titleText + $captionText);\n      }\n      items.push(item);\n    });\n\n    // bind click event\n    $folioItems.each(function (i) {\n      $(this).find('.folio-item__thumb-link').on('click', function (e) {\n        e.preventDefault();\n        var options = {\n          index: i,\n          showHideOpacity: true\n        };\n\n        // initialize PhotoSwipe\n        var lightBox = new PhotoSwipe($pswp, PhotoSwipeUI_Default, items, options);\n        lightBox.init();\n      });\n    });\n  };\n\n  /* slick slider\n   * ------------------------------------------------------ */\n  var ssSlickSlider = function ssSlickSlider() {\n    $('.testimonial-slider').slick({\n      arrows: false,\n      dots: true,\n      infinite: true,\n      slidesToShow: 1,\n      slidesToScroll: 1,\n      pauseOnFocus: false,\n      autoplaySpeed: 1500,\n      responsive: [{\n        breakpoint: 1080,\n        settings: {\n          slidesToShow: 1,\n          slidesToScroll: 1\n        }\n      }, {\n        breakpoint: 800,\n        settings: {\n          slidesToShow: 1,\n          slidesToScroll: 1\n        }\n      }]\n    });\n  };\n\n  /* alert boxes\n   * ------------------------------------------------------ */\n  var ssAlertBoxes = function ssAlertBoxes() {\n    $('.alert-box').on('click', '.alert-box__close', function () {\n      $(this).parent().fadeOut(500);\n    });\n  };\n\n  /* smooth scrolling\n   * ------------------------------------------------------ */\n  var ssSmoothScroll = function ssSmoothScroll() {\n    $('.smoothscroll').on('click', function (e) {\n      var target = this.hash;\n      var $target = $(target);\n      e.preventDefault();\n      e.stopPropagation();\n      $('html, body').stop().animate({\n        'scrollTop': $target.offset().top\n      }, cfg.scrollDuration, 'swing').promise().done(function () {\n        window.location.hash = target;\n      });\n    });\n  };\n\n  /* back to top\n   * ------------------------------------------------------ */\n  var ssBackToTop = function ssBackToTop() {\n    var pxShow = 800;\n    var $goTopButton = $(\".ss-go-top\");\n\n    // Show or hide the button\n    if ($(window).scrollTop() >= pxShow) $goTopButton.addClass('link-is-visible');\n    $(window).on('scroll', function () {\n      if ($(window).scrollTop() >= pxShow) {\n        if (!$goTopButton.hasClass('link-is-visible')) $goTopButton.addClass('link-is-visible');\n      } else {\n        $goTopButton.removeClass('link-is-visible');\n      }\n    });\n  };\n\n  /* initialize\n   * ------------------------------------------------------ */\n  (function ssInit() {\n    ssPrettyPrint();\n    ssMoveHeader();\n    ssMobileMenu();\n    ssAccordion();\n    ssPhotoswipe();\n    ssSlickSlider();\n    ssAlertBoxes();\n    ssSmoothScroll();\n    ssBackToTop();\n  })();\n})(jQuery);\n\n//# sourceURL=webpack://blank-theme/./assets/src/js/main.js?");

/***/ }),

/***/ "./node_modules/@babel/runtime/helpers/esm/classCallCheck.js":
/*!*******************************************************************!*\
  !*** ./node_modules/@babel/runtime/helpers/esm/classCallCheck.js ***!
  \*******************************************************************/
/***/ ((__unused_webpack___webpack_module__, __webpack_exports__, __webpack_require__) => {

eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"default\": () => (/* binding */ _classCallCheck)\n/* harmony export */ });\nfunction _classCallCheck(instance, Constructor) {\n  if (!(instance instanceof Constructor)) {\n    throw new TypeError(\"Cannot call a class as a function\");\n  }\n}\n\n//# sourceURL=webpack://blank-theme/./node_modules/@babel/runtime/helpers/esm/classCallCheck.js?");

/***/ }),

/***/ "./node_modules/@babel/runtime/helpers/esm/createClass.js":
/*!****************************************************************!*\
  !*** ./node_modules/@babel/runtime/helpers/esm/createClass.js ***!
  \****************************************************************/
/***/ ((__unused_webpack___webpack_module__, __webpack_exports__, __webpack_require__) => {

eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"default\": () => (/* binding */ _createClass)\n/* harmony export */ });\n/* harmony import */ var _toPropertyKey_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./toPropertyKey.js */ \"./node_modules/@babel/runtime/helpers/esm/toPropertyKey.js\");\n\nfunction _defineProperties(target, props) {\n  for (var i = 0; i < props.length; i++) {\n    var descriptor = props[i];\n    descriptor.enumerable = descriptor.enumerable || false;\n    descriptor.configurable = true;\n    if (\"value\" in descriptor) descriptor.writable = true;\n    Object.defineProperty(target, (0,_toPropertyKey_js__WEBPACK_IMPORTED_MODULE_0__[\"default\"])(descriptor.key), descriptor);\n  }\n}\nfunction _createClass(Constructor, protoProps, staticProps) {\n  if (protoProps) _defineProperties(Constructor.prototype, protoProps);\n  if (staticProps) _defineProperties(Constructor, staticProps);\n  Object.defineProperty(Constructor, \"prototype\", {\n    writable: false\n  });\n  return Constructor;\n}\n\n//# sourceURL=webpack://blank-theme/./node_modules/@babel/runtime/helpers/esm/createClass.js?");

/***/ }),

/***/ "./node_modules/@babel/runtime/helpers/esm/toPrimitive.js":
/*!****************************************************************!*\
  !*** ./node_modules/@babel/runtime/helpers/esm/toPrimitive.js ***!
  \****************************************************************/
/***/ ((__unused_webpack___webpack_module__, __webpack_exports__, __webpack_require__) => {

eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"default\": () => (/* binding */ _toPrimitive)\n/* harmony export */ });\n/* harmony import */ var _typeof_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./typeof.js */ \"./node_modules/@babel/runtime/helpers/esm/typeof.js\");\n\nfunction _toPrimitive(input, hint) {\n  if ((0,_typeof_js__WEBPACK_IMPORTED_MODULE_0__[\"default\"])(input) !== \"object\" || input === null) return input;\n  var prim = input[Symbol.toPrimitive];\n  if (prim !== undefined) {\n    var res = prim.call(input, hint || \"default\");\n    if ((0,_typeof_js__WEBPACK_IMPORTED_MODULE_0__[\"default\"])(res) !== \"object\") return res;\n    throw new TypeError(\"@@toPrimitive must return a primitive value.\");\n  }\n  return (hint === \"string\" ? String : Number)(input);\n}\n\n//# sourceURL=webpack://blank-theme/./node_modules/@babel/runtime/helpers/esm/toPrimitive.js?");

/***/ }),

/***/ "./node_modules/@babel/runtime/helpers/esm/toPropertyKey.js":
/*!******************************************************************!*\
  !*** ./node_modules/@babel/runtime/helpers/esm/toPropertyKey.js ***!
  \******************************************************************/
/***/ ((__unused_webpack___webpack_module__, __webpack_exports__, __webpack_require__) => {

eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"default\": () => (/* binding */ _toPropertyKey)\n/* harmony export */ });\n/* harmony import */ var _typeof_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./typeof.js */ \"./node_modules/@babel/runtime/helpers/esm/typeof.js\");\n/* harmony import */ var _toPrimitive_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./toPrimitive.js */ \"./node_modules/@babel/runtime/helpers/esm/toPrimitive.js\");\n\n\nfunction _toPropertyKey(arg) {\n  var key = (0,_toPrimitive_js__WEBPACK_IMPORTED_MODULE_1__[\"default\"])(arg, \"string\");\n  return (0,_typeof_js__WEBPACK_IMPORTED_MODULE_0__[\"default\"])(key) === \"symbol\" ? key : String(key);\n}\n\n//# sourceURL=webpack://blank-theme/./node_modules/@babel/runtime/helpers/esm/toPropertyKey.js?");

/***/ }),

/***/ "./node_modules/@babel/runtime/helpers/esm/typeof.js":
/*!***********************************************************!*\
  !*** ./node_modules/@babel/runtime/helpers/esm/typeof.js ***!
  \***********************************************************/
/***/ ((__unused_webpack___webpack_module__, __webpack_exports__, __webpack_require__) => {

eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"default\": () => (/* binding */ _typeof)\n/* harmony export */ });\nfunction _typeof(obj) {\n  \"@babel/helpers - typeof\";\n\n  return _typeof = \"function\" == typeof Symbol && \"symbol\" == typeof Symbol.iterator ? function (obj) {\n    return typeof obj;\n  } : function (obj) {\n    return obj && \"function\" == typeof Symbol && obj.constructor === Symbol && obj !== Symbol.prototype ? \"symbol\" : typeof obj;\n  }, _typeof(obj);\n}\n\n//# sourceURL=webpack://blank-theme/./node_modules/@babel/runtime/helpers/esm/typeof.js?");

/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		var cachedModule = __webpack_module_cache__[moduleId];
/******/ 		if (cachedModule !== undefined) {
/******/ 			return cachedModule.exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			// no module.id needed
/******/ 			// no module.loaded needed
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId](module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/************************************************************************/
/******/ 	/* webpack/runtime/define property getters */
/******/ 	(() => {
/******/ 		// define getter functions for harmony exports
/******/ 		__webpack_require__.d = (exports, definition) => {
/******/ 			for(var key in definition) {
/******/ 				if(__webpack_require__.o(definition, key) && !__webpack_require__.o(exports, key)) {
/******/ 					Object.defineProperty(exports, key, { enumerable: true, get: definition[key] });
/******/ 				}
/******/ 			}
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/hasOwnProperty shorthand */
/******/ 	(() => {
/******/ 		__webpack_require__.o = (obj, prop) => (Object.prototype.hasOwnProperty.call(obj, prop))
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/make namespace object */
/******/ 	(() => {
/******/ 		// define __esModule on exports
/******/ 		__webpack_require__.r = (exports) => {
/******/ 			if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 				Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 			}
/******/ 			Object.defineProperty(exports, '__esModule', { value: true });
/******/ 		};
/******/ 	})();
/******/ 	
/************************************************************************/
/******/ 	
/******/ 	// startup
/******/ 	// Load entry module and return exports
/******/ 	// This entry module can't be inlined because the eval devtool is used.
/******/ 	var __webpack_exports__ = __webpack_require__("./assets/src/js/main.js");
/******/ 	
/******/ })()
;