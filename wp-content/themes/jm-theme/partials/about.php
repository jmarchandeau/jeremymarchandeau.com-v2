<?php
$section_about_subhead = get_post_meta( get_the_ID(), '_jm_section_about_subhead', true );
$section_about_title = get_post_meta( get_the_ID(), '_jm_section_about_title', true );
$picture = get_post_meta( get_the_ID(), '_jm_picture', true );
$profile = get_post_meta( get_the_ID(), '_jm_profile', true );
$career = get_post_meta( get_the_ID(), '_jm_career', true );

?>

<section id="about" class="s-about target-section">

<div class="s-about__section s-about__section--profile">

    <div class="right-vert-line"></div>

    <div class="row">
        <div class="column large-6 medium-8 tab-full">

            <div class="section-intro" data-num="01">
            <?php if ( ! empty( $section_about_subhead ) ) {
                    echo '<h3 class="subhead">' . esc_html( $section_about_subhead ) . '</h3>';
                } ?>
            <?php if ( ! empty( $section_about_title ) ) {
                    echo '<h2 class="display-1">' . esc_html( $section_about_title ) . '</h2>';
                } ?>
            </div>
            <?php if ( ! empty( $picture ) ) { ?>
                <div class="profile-pic">
                    <img src="<?php jm_image_path( $picture ); ?>" 
                     srcset="<?php jm_image_path( $picture ); ?> 1x, <?php jm_image_path( $picture ); ?> 2x" alt="" style="border-radius:50%">
                </div>
                <?php } ?>


            <h3>Profile</h3>
            <div>
            <?php if ( ! empty( $profile ) ) {
                    echo wpautop( wp_kses_post( $profile ) );
                } ?>
            </div>
        </div>
    </div>

</div> <!-- end s-about__section--profile -->

<div class="s-about__section">

    <div class="row">
        <div class="column">
            <h3>Career</h3>
        </div>
    </div>

    <?php
    if ( ! empty( $career ) ) { ?>
        <div class="row block-large-1-2 block-900-full work-positions">
        <?php
        foreach ( $career as $job ) { ?>
            <div class="column">
                <div class="position">
                    <div class="position__header">
                        <h6>
                            <span class="position__co"><?php echo esc_html( $job['_jm_company'] ); ?></span>
                            <span class="position__pos"><?php echo esc_html( $job['_jm_job'] ); ?></span>
                        </h6>
                        <div class="position__timeframe">
                        <?php echo esc_html( $job['_jm_period'] ); ?>
                        </div>
                    </div>

                    <?php echo wpautop( wp_kses_post( $job['_jm_description'] ) ); ?>
                </div>
            </div> <!-- end column -->
        <?php } ?>
        </div> <!-- work positions -->
    <?php }
    ?>

</div> <!-- end s-about__section -->

</section> <!-- end s-about -->