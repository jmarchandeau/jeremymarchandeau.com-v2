<?php
$args = array(
	'post_type' => 'jm_testimonials',
	'post_status' => 'publish',
	'posts_per_page' => -1,
	'order' => 'ASC',
	'orderby' => 'date'
);

$testimonials = new WP_Query( $args );

if ( $testimonials->have_posts() ) { ?>


<div class="row collapse">

    <div class="column large-full">

        <div class="testimonial-slider">

        <?php
            while ( $testimonials->have_posts() ) {
                $testimonials->the_post();

                // Get the fields using CMB2
                $role = get_post_meta( get_the_ID(), '_jm_testimonials_role', true );
                $picture = get_post_meta( get_the_ID(), '_jm_testimonials_picture', true );
                $testimonial = get_post_meta( get_the_ID(), '_jm_testimonials_testimonial', true ); ?>

                <div class="testimonial-slider__slide">
                    
                    <blockquote><span class="bqstart">“</span><?php echo $testimonial; ?><span class="bqend">”</span></blockquote>
                    
                    <div class="testimonial-slider__author">
                        <img src="<?php jm_image_path( $picture ); ?>" alt="<?php echo get_the_title(); ?> image" class="testimonial-slider__avatar">
                        <cite class="testimonial-slider__cite">
                            <strong><?php echo get_the_title(); ?></strong>
                            <span><?php echo $role; ?></span>
                        </cite>
                    </div>
                </div> <!-- end testimonials__slide -->
            <?php
            }
            wp_reset_postdata(); ?>

        </div> <!-- end testimonial slider -->
        
    </div> <!-- end column -->
    
</div> <!-- end row -->

<?php    
}