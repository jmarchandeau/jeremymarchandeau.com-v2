<?php
        // Retrieve the social networks data from the options page
        $cta_title = get_option( 'jm_cta_options' )['jm_cta_title'];
        $cta_subtitle = get_option( 'jm_cta_options' )['jm_cta_subtitle'];
        $cta_button_title = get_option( 'jm_cta_options' )['jm_cta_button_title'];
        $cta_button_link = get_option( 'jm_cta_options' )['jm_cta_button_link'];

        // Loop through each social network and output its name and URL
        if ( $cta_title ): ?>
        <div class="s-cta">

            <div class="row">
                <div class="column large-full">
                    <h2 class="section-desc">
                        <?php echo nl2br( esc_html( $cta_title ) ); ?>
                    </h2>
                </div>
            </div>

            <div class="row cta-content">
                <div class="column large-full">
                    <?php echo wpautop( wp_kses_post( $cta_subtitle ) ); ?>

                    <a href="<?php echo esc_html( $cta_button_link ); ?>" class="btn btn--primary h-full-width"><?php echo nl2br( esc_html( $cta_button_title ) ); ?></a>
                </div>
            </div> <!-- end cta-content -->

        </div> <!-- end s-cta -->

        <?php
        endif;