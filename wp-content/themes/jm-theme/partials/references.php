<?php
$section_references_subhead = get_post_meta( get_the_ID(), '_jm_section_references_subhead', true );
$section_references_title = get_post_meta( get_the_ID(), '_jm_section_references_title', true );
$references = get_post_meta( get_the_ID(), '_jm_references', true );
?>

<section id="portfolio" class="s-portfolio target-section">

<div class="row s-portfolio__header">
    <div class="column large-6 medium-8 tab-full">
        <div class="section-intro" data-num="03">
        <?php if ( ! empty( $section_references_subhead ) ) {
                    echo '<h3 class="subhead">' . esc_html( $section_references_subhead ) . '</h3>';
                } ?>
                <?php if ( ! empty( $section_references_title ) ) {
                    echo '<h2 class="display-1">' . esc_html( $section_references_title ) . '</h2>';
                } ?>
        </div>
    </div>
</div> <!-- s-porfolio__header -->

<?php if ( ! empty( $references ) ) { ?>
<div class="row s-porfolio__list block-large-1-2 block-tab-full collapse">
<?php foreach ( $references as $reference ) { ?>
    <div class="column">
        <div class="folio-item">
            <div class="folio-item__thumb">
                <a class="folio-item__thumb-link" href="<?php jm_image_path( $reference['_jm_project_image'] ); ?>" title="<?php echo esc_html( $reference['_jm_project'] ); ?>" data-size="1050x700">
                    <img src="<?php jm_image_path( $reference['_jm_project_image'] ); ?>" width="600px" height="400px" 
                         srcset="<?php jm_image_path( $reference['_jm_project_image'] ); ?> 1x, <?php jm_image_path( $reference['_jm_project_image'] ); ?> 2x" alt="">
                </a>
            </div>
            <div class="folio-item__info">
                <div class="folio-item__cat"><?php echo esc_html( $reference['_jm_mission'] ); ?></div>
                <h4 class="folio-item__title"><?php echo esc_html( $reference['_jm_project'] ); ?></h4>
            </div>
            <a href="<?php echo esc_html( $reference['_jm_project_link'] ); ?>" title="Project Link" class="folio-item__project-link" target="_blank">Project Link</a>
            <div class="folio-item__caption">
            <?php echo wpautop( wp_kses_post( $reference['_jm_project_details'] ) ); ?>
            </div>
        </div>
    </div> <!-- end column -->
<?php } ?>

</div> <!-- folio-list -->
<?php } ?>

</section> <!-- end portfolio -->