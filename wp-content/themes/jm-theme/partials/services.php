<?php
$section_services_subhead = get_post_meta( get_the_ID(), '_jm_section_expertise_subhead', true );
$section_services_title = get_post_meta( get_the_ID(), '_jm_section_expertise_title', true );
$intro = get_post_meta( get_the_ID(), '_jm_intro', true );
$services = get_post_meta( get_the_ID(), '_jm_services', true );
?>
    <section id="services" class="s-services target-section h-dark-bg">
        
        <div class="row s-services__content">

            <div class="vert-line"></div>

            <div class="column large-6 s-services__leftcol">
                <div class="section-intro" data-num="02">
                <?php if ( ! empty( $section_services_subhead ) ) {
                    echo '<h3 class="subhead">' . esc_html( $section_services_subhead ) . '</h3>';
                } ?>
                <?php if ( ! empty( $section_services_title ) ) {
                        echo '<h2 class="display-1">' . esc_html( $section_services_title ) . '</h2>';
                } ?>
                </div>

                <p class="lead">
                <?php if ( ! empty( $intro ) ) {
                    echo wp_kses_post( $intro );
                } ?>
                </p>
            </div> <!-- end  s-services--leftcol -->
            <?php
            if ( ! empty( $services ) ) { ?>
            <div class="column large-6">
                <ul class="services-list">
                <?php
                foreach ( $services as $service ) { ?>
                    <li class="services-list__item">
                        <div class="services-list__item-header">
                            <h5><?php echo esc_html( $service['_jm_service'] ); ?></h5>
                        </div>
                        <div class="services-list__item-body">
                        <?php echo wpautop( wp_kses_post( $service['_jm_service_description'] ) ); ?>
                        </div>
                    </li> <!-- services-list__item -->
                    <?php } ?>
                </ul> <!-- end services-list -->
            </div>
            <?php }
            ?>

        </div> <!-- s-services__content -->

    </section> <!-- end s-services -->