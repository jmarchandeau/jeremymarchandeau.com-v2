<?php
// Get the ID of the front page
$front_page_id = get_option( 'page_on_front' );

// Get the values of the background image and title fields using get_post_meta()
$background_image = get_post_meta( $front_page_id, '_jm_background_image', true );
$title = get_post_meta( $front_page_id, '_jm_title', true );
?>

<section id="hero" class="s-hero target-section" data-parallax="scroll" data-image-src="<?php jm_image_path( $background_image ); ?>" data-natural-width=3000 data-natural-height=2000 data-position-y=center>

<div class="row hero-content">

    <div class="column large-full">

            <!-- Output the title -->
        <?php if ( $title ) : ?>
            <h1><?php echo nl2br( esc_html( $title ) ); ?></h1>
        <?php endif; ?>
        
        <?php
        echo do_shortcode('[social_networks class="hero-social" target="_blank"]');
        ?>     

    </div> 

</div> <!-- end hero-content -->

<div class="hero-scroll">
    <a href="#about" class="scroll-link smoothscroll">
        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M12 24l-8-9h6v-15h4v15h6z"/></svg>
    </a>
</div> <!-- end hero-scroll -->

</section> <!-- end s-hero -->