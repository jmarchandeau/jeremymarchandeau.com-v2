<?php

// Custom Post types & Taxonomies here

    
function jm_testimonials() {

    $labels = array(
      'name'                  => _x( 'Testimonials', 'Post Type General Name', 'jm-theme' ),
      'singular_name'         => _x( 'Testimonial', 'Post Type Singular Name', 'jm-theme' ),
      'menu_name'             => __( 'Testimonials', 'jm-theme' ),
      'name_admin_bar'        => __( 'Testimonials', 'jm-theme' ),
      'archives'              => __( 'Testimonial Archives', 'jm-theme' ),
      'attributes'            => __( 'Post Attributes', 'jm-theme' ),
      'parent_item_colon'     => __( 'Parent Post', 'jm-theme' ),
      'all_items'             => __( 'All Testimonials', 'jm-theme' ),
      'add_new_item'          => __( 'Add New Testimonial', 'jm-theme' ),
      'add_new'               => __( 'Add New', 'jm-theme' ),
      'new_item'              => __( 'New Testimonial', 'jm-theme' ),
      'edit_item'             => __( 'Edit Testimonial', 'jm-theme' ),
      'update_item'           => __( 'Update Testimonial', 'jm-theme' ),
      'view_item'             => __( 'View Testimonial', 'jm-theme' ),
      'view_items'            => __( 'View Posts', 'jm-theme' ),
      'search_items'          => __( 'Search Post', 'jm-theme' ),
      'not_found'             => __( 'Not Found', 'jm-theme' ),
      'not_found_in_trash'    => __( 'No found in Trash', 'jm-theme' ),
      'featured_image'        => __( 'Featured Image', 'jm-theme' ),
      'set_featured_image'    => __( 'Set Featured Image', 'jm-theme' ),
      'remove_featured_image' => __( 'Remove Featured Image', 'jm-theme' ),
      'use_featured_image'    => __( 'Use as Featured Image', 'jm-theme' ),
      'insert_into_item'      => __( 'Insert into Post', 'jm-theme' ),
      'uploaded_to_this_item' => __( 'Uploaded to this Post', 'jm-theme' ),
      'items_list'            => __( 'Posts List', 'jm-theme' ),
      'items_list_navigation' => __( 'Posts List Navigation', 'jm-theme' ),
      'filter_items_list'     => __( 'Filter Posts List', 'jm-theme' ),
    );

    $args = array(
      'label'                 => __( 'Testimonials', 'jm-theme' ),
      'description'           => __( 'Client Testimonials', 'jm-theme' ),
      'labels'                => $labels,
      'supports'              => array( 'title', 'page-attributes' ),
      'hierarchical'          => true,
      'public'                => false,
      'show_ui'               => true,
      'show_in_menu'          => true,
      'menu_position'         => 20,
      'menu_icon'             => 'dashicons-format-quote',
      'show_in_admin_bar'     => true,
      'show_in_rest'          => true,
      'show_in_nav_menus'     => true,
      'can_export'            => true,
      'has_archive'           => false,
      'exclude_from_search'   => true,
      'publicly_queryable'    => false,
      'capability_type'       => 'post',
      'rewrite'               => true,
    );
    register_post_type( 'jm_testimonials', $args );

  }
  add_action( 'init', 'jm_testimonials', 0 );


