<?php
/*
 *
 * Home Page Metaboxes
 * 
 */
add_action( 'cmb2_init', 'jm_cmb2_add_metabox' );
function jm_cmb2_add_metabox() {

	$prefix = '_jm_';

	$cmb = new_cmb2_box( array(
		'id'           => $prefix . 'hero_banner',
		'title'        => __( 'Hero Banner', 'jm-theme' ),
		'object_types' => array( 'page' ),
		'context'      => 'normal',
		'priority'     => 'default',
        'show_on'      => array(
            'key'      => 'id',
            'value'    => get_option( 'page_on_front' ),
        ),
	) );

	$cmb->add_field( array(
		'name' => __( 'Background Image', 'jm-theme' ),
		'id' => $prefix . 'background_image',
		'type' => 'file',
	) );

	$cmb->add_field( array(
		'name' => __( 'Title', 'jm-theme' ),
		'id' => $prefix . 'title',
		'type' => 'textarea_small',
	) );

}
/* About Section */
add_action( 'cmb2_init', 'jm_cmb2_add_section_about_metabox' );
function jm_cmb2_add_section_about_metabox() {

	$prefix = '_jm_';

	$cmb = new_cmb2_box( array(
		'id'           => $prefix . 'section_about',
		'title'        => __( 'Section About', 'jm-theme' ),
		'object_types' => array( 'page' ),
		'context'      => 'normal',
		'priority'     => 'default',
        'show_on'      => array(
            'key'      => 'id',
            'value'    => get_option( 'page_on_front' ),
        ),
	) );

	$cmb->add_field( array(
		'name' => __( 'Section About subhead', 'jm-theme' ),
		'id' => $prefix . 'section_about_subhead',
		'type' => 'text_small',
	) );

	$cmb->add_field( array(
		'name' => __( 'Section About title', 'jm-theme' ),
		'id' => $prefix . 'section_about_title',
		'type' => 'textarea_small',
	) );

	$cmb->add_field( array(
		'name' => __( 'Picture', 'jm-theme' ),
		'id' => $prefix . 'picture',
		'type' => 'file',
	) );

	$cmb->add_field( array(
		'name' => __( 'Profile', 'jm-theme' ),
		'id' => $prefix . 'profile',
		'type' => 'wysiwyg',
	) );

	$career = $cmb->add_field( array(
		'name' => __( 'Career', 'jm-theme' ),
		'id' => $prefix . 'career',
		'type' => 'group',
	) );

		$cmb->add_group_field( $career, array(
			'name' => __( 'Company', 'jm-theme' ),
			'id' => $prefix . 'company',
			'type' => 'text_small',
		) );

		$cmb->add_group_field( $career, array(
			'name' => __( 'Job', 'jm-theme' ),
			'id' => $prefix . 'job',
			'type' => 'text',
		) );

		$cmb->add_group_field( $career, array(
			'name' => __( 'Period', 'jm-theme' ),
			'id' => $prefix . 'period',
			'type' => 'text',
		) );

		$cmb->add_group_field( $career, array(
			'name' => __( 'Description', 'jm-theme' ),
			'id' => $prefix . 'description',
			'type' => 'wysiwyg',
		) );

}

/* Expertise Section */
add_action( 'cmb2_init', 'jm_cmb2_add_section_expertise_metabox' );
function jm_cmb2_add_section_expertise_metabox() {

	$prefix = '_jm_';

	$cmb = new_cmb2_box( array(
		'id'           => $prefix . 'section_expertise',
		'title'        => __( 'Section Expertise', 'jm-theme' ),
		'object_types' => array( 'page' ),
		'context'      => 'normal',
		'priority'     => 'default',
        'show_on'      => array(
            'key'      => 'id',
            'value'    => get_option( 'page_on_front' ),
        ),
	) );

	$cmb->add_field( array(
		'name' => __( 'Section Expertise subhead', 'jm-theme' ),
		'id' => $prefix . 'section_expertise_subhead',
		'type' => 'text_small',
	) );

	$cmb->add_field( array(
		'name' => __( 'Section Expertise title', 'jm-theme' ),
		'id' => $prefix . 'section_expertise_title',
		'type' => 'textarea_small',
	) );

	$cmb->add_field( array(
		'name' => __( 'Intro', 'jm-theme' ),
		'id' => $prefix . 'intro',
		'type' => 'wysiwyg',
	) );

	$career = $cmb->add_field( array(
		'name' => __( 'Services', 'jm-theme' ),
		'id' => $prefix . 'services',
		'type' => 'group',
	) );

		$cmb->add_group_field( $career, array(
			'name' => __( 'Service', 'jm-theme' ),
			'id' => $prefix . 'service',
			'type' => 'text',
		) );

		$cmb->add_group_field( $career, array(
			'name' => __( 'Description', 'jm-theme' ),
			'id' => $prefix . 'service_description',
			'type' => 'textarea',
		) );

}


/* References Section */
add_action( 'cmb2_init', 'jm_cmb2_add_section_references_metabox' );
function jm_cmb2_add_section_references_metabox() {

	$prefix = '_jm_';

	$cmb = new_cmb2_box( array(
		'id'           => $prefix . 'section_references',
		'title'        => __( 'Section References', 'jm-theme' ),
		'object_types' => array( 'page' ),
		'context'      => 'normal',
		'priority'     => 'default',
        'show_on'      => array(
            'key'      => 'id',
            'value'    => get_option( 'page_on_front' ),
        ),
	) );

	$cmb->add_field( array(
		'name' => __( 'Section References subhead', 'jm-theme' ),
		'id' => $prefix . 'section_references_subhead',
		'type' => 'text_small',
	) );

	$cmb->add_field( array(
		'name' => __( 'Section References title', 'jm-theme' ),
		'id' => $prefix . 'section_references_title',
		'type' => 'textarea_small',
	) );

	$references = $cmb->add_field( array(
		'name' => __( 'References', 'jm-theme' ),
		'id' => $prefix . 'references',
		'type' => 'group',
	) );

		$cmb->add_group_field( $references, array(
			'name' => __( 'Project', 'jm-theme' ),
			'id' => $prefix . 'project',
			'type' => 'text',
		) );

		$cmb->add_group_field( $references, array(
			'name' => __( 'Mission', 'jm-theme' ),
			'id' => $prefix . 'mission',
			'type' => 'text',
		) );

		$cmb->add_group_field( $references, array(
			'name' => __( 'Project Link', 'jm-theme' ),
			'id' => $prefix . 'project_link',
			'type' => 'text_url',
		) );

		$cmb->add_group_field( $references, array(
			'name' => __( 'Project Details', 'jm-theme' ),
			'id' => $prefix . 'project_details',
			'type' => 'textarea',
		) );

		$cmb->add_group_field( $references, array(
			'name' => __( 'Project Image', 'jm-theme' ),
			'id' => $prefix . 'project_image',
			'type' => 'file',
		) );

}


/*
 *
 * Testimonials Metaboxes
 * 
 */
add_action( 'cmb2_init', 'jm_add_testimonials_metabox' );
function jm_add_testimonials_metabox() {

	$prefix = '_jm_testimonials_';

	$cmb = new_cmb2_box( array(
		'id'           => $prefix . 'testimonials_metabox',
		'title'        => __( 'Testimonials Metabox', 'jm-theme' ),
		'object_types' => array( 'jm_testimonials' ),
		'context'      => 'normal',
		'priority'     => 'default',
	) );

	$cmb->add_field( array(
		'name' => __( 'Role', 'jm-theme' ),
		'id' => $prefix . 'role',
		'type' => 'text',
	) );

	$cmb->add_field( array(
		'name' => __( 'Picture', 'jm-theme' ),
		'id' => $prefix . 'picture',
		'type' => 'file',
	) );

	$cmb->add_field( array(
		'name' => __( 'Testimonial', 'jm-theme' ),
		'id' => $prefix . 'testimonial',
		'type' => 'textarea',
	) );

}