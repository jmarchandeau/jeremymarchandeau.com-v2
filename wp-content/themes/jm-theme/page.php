<?php
get_header();
?>

    <?php
    while(have_posts()) : the_post();
        ?>

        <section class="s-legal target-section" style="padding-top:150px;">

        <div class="row collapse">

            <div class="column large-full">

            <?php the_title('<h1>', '</h1>'); ?>
            </br>

            <?php
            the_content();
            ?>

            </div>

        </div>

        </section>

        <?php
    endwhile; // End of the loop.
    ?>

<?php
get_footer();

