<!DOCTYPE HTML>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<title><?php wp_title(); ?></title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="icon" type="image/png" sizes="32x32" href="<?php echo get_stylesheet_directory_uri(); ?>/favicon.png">
<link rel="icon" type="image/png" sizes="16x16" href="<?php echo get_stylesheet_directory_uri(); ?>/favicon.png">
<link rel="profile" href="http://gmpg.org/xfn/11">
<?php wp_head(); ?>
</head>

<body <?php body_class(); ?> id="top">
<?php wp_body_open(); ?>
    
    <!-- header
    ================================================== -->
    <header class="s-header">

        <div class="header-logo">
            <a href="<?php echo esc_url( home_url( '/' ) ); ?>">
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/dist/img/logo_jm_white_transparent_bg.png" alt="Homepage" width="300px" height="38px">
            </a>
        </div>

        <div class="header-content">
    
            <nav class="row header-nav-wrap">
                <ul class="header-nav">
                    <li><a class="smoothscroll" href="<?php echo esc_url( home_url( '/' ) ); ?>#hero" title="Intro">Home</a></li>
                    <li><a class="smoothscroll" href="<?php echo esc_url( home_url( '/' ) ); ?>#about" title="About">About</a></li>
                    <li><a class="smoothscroll" href="<?php echo esc_url( home_url( '/' ) ); ?>#services" title="Services">Skills</a></li>
                    <li><a class="smoothscroll" href="<?php echo esc_url( home_url( '/' ) ); ?>#portfolio" title="References">References</a></li>
                    
                </ul>
            </nav> <!-- end header-nav-wrap -->

            <button class="btn btn--stroke btn--small contact-me">Contact</button>

        </div> <!-- end header-content -->

        <a class="header-menu-toggle" href="#0"><span>Menu</span></a>

    </header> 
