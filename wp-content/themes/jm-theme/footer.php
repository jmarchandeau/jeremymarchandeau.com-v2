<!-- footer
================================================== -->
<footer class="s-footer h-dark-bg">
    
    <div id="footer-intro" class="right-vert-line"></div>
    <?php if( is_front_page() ): ?>
    <div class="row s-footer__main">
        <div class="column large-6">
            <div class="section-intro">

                    <?php
                // Retrieve the social networks data from the options page
                $footer_cta_title = get_option( 'jm_footer_options' )['jm_footer_cta_title'];
                $footer_cta_subtitle = get_option( 'jm_footer_options' )['jm_footer_cta_subtitle'];
                $footer_cta_button_title = get_option( 'jm_footer_options' )['jm_footer_cta_button_title'];
                $footer_cta_button_link = get_option( 'jm_footer_options' )['jm_footer_cta_button_link'];

                // Loop through each social network and output its name and URL
                if ( $footer_cta_title ): ?>
                <h3 class="subhead"><?php echo nl2br( esc_html( $footer_cta_title ) ); ?></h3>
                <h2 class="display-1">
                    <?php echo wpautop( wp_kses_post( $footer_cta_subtitle ) ); ?>
                </h2>
                <?php endif; ?>
            </div>

            <div class="footer-email-us">
                <button class="btn btn--primary h-full-width contact-me"><?php echo nl2br( esc_html( $footer_cta_button_title ) ); ?></button>
            </div>
        </div>

        <div class="column large-5">
            <div class="footer-contacts">
            <?php $email_address = get_option( 'jm_contact_details' )['jm_email_address']; 
                if ( $email_address ): ?>
                <div class="footer-contact-block">
                    <h5 class="footer-contact-block__header">
                        Email
                    </h5>
                    <p class="footer-contact-block__content">
                        <a href="mailto:<?php echo esc_html( $email_address ); ?>"><?php echo esc_html( $email_address ); ?></a>
                    </p>
                </div> <!-- end footer-contact-block -->
                <?php endif; ?>
                <br>
                <div class="footer-contact-block">
                    <h5 class="footer-contact-block__header">
                        Social
                    </h5>
                    <?php
                    echo do_shortcode('[social_networks class="footer-contact-block__list" target="_blank"]');
                    ?>  
                </div> <!-- end footer-contact-block -->
            </div>
        </div>
    </div> <!-- end s-footer__main -->
    <?php endif; ?>
    
    <div class="row s-footer__bottom">
        <div class="column large-full ss-copyright">
            <span>© Copyright JM <?php echo date ("Y"); ?></span>
            <?php
                $menu_items = wp_get_nav_menu_items( 'footer-menu' ); // Change 'menu-footer' to the name or ID of your footer menu
                if ( ! empty( $menu_items ) ) {
                    foreach ( $menu_items as $menu_item ) {
                        echo '<span><a href="' . esc_url( $menu_item->url ) . '">' . esc_html( $menu_item->title ) . '</a></span>';
                    }
                }
                ?>

        </div> <!-- end ss-copyright -->

        <div class="ss-go-top">
            <a class="smoothscroll" title="Back to Top" href="#top">
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M12 0l8 9h-6v15h-4v-15h-6z"/></svg>
            </a>
        </div> <!-- end ss-go-top -->
    </div> <!-- end s-footer__bottom -->

</footer> <!-- end s-footer -->


<!-- photoswipe background
================================================== -->
<div aria-hidden="true" class="pswp" role="dialog" tabindex="-1">

    <div class="pswp__bg"></div>
    <div class="pswp__scroll-wrap">

        <div class="pswp__container">
            <div class="pswp__item"></div>
            <div class="pswp__item"></div>
            <div class="pswp__item"></div>
        </div>

        <div class="pswp__ui pswp__ui--hidden">
            <div class="pswp__top-bar">
                <div class="pswp__counter"></div><button class="pswp__button pswp__button--close" title="Close (Esc)"></button> <button class="pswp__button pswp__button--share" title=
                "Share"></button> <button class="pswp__button pswp__button--fs" title="Toggle fullscreen"></button> <button class="pswp__button pswp__button--zoom" title=
                "Zoom in/out"></button>
                <div class="pswp__preloader">
                    <div class="pswp__preloader__icn">
                        <div class="pswp__preloader__cut">
                            <div class="pswp__preloader__donut"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
                <div class="pswp__share-tooltip"></div>
            </div><button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)"></button> <button class="pswp__button pswp__button--arrow--right" title=
            "Next (arrow right)"></button>
            <div class="pswp__caption">
                <div class="pswp__caption__center"></div>
            </div>
        </div>

    </div>

</div> <!-- end photoSwipe background -->


<div id="contact-popup" class="popup">
<div class="popup-content">
    <span class="close">&times;</span>
    <h2>Contact Me</h2>
    <?php echo do_shortcode('[contact-form-7 id="33" title="Contact form 1"]'); ?>
</div>
</div>


<?php wp_footer(); ?>

</body>
</html>