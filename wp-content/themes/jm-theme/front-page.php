<?php
get_header();
?>

    <?php
    while(have_posts()) : the_post();
        ?>

            <?php
            the_content();
            ?>

    
        <!-- hero
        ================================================== -->
        <?php include('partials/hero.php'); ?>

        <!-- about
        ================================================== -->
        <?php include('partials/about.php'); ?>

        <!-- services
        ================================================== -->
        <?php include('partials/services.php'); ?>

        <!-- portfolio
        ================================================== -->
        <?php include('partials/references.php'); ?>

        <section id="testimonials" class="s-testimonials">

        <!-- testimonials
        ================================================== -->        
        <?php include('partials/testimonials.php'); ?>

        <!-- CTA
        ================================================== -->
        <?php include('partials/cta.php'); ?>

        </section> <!-- end s-testimonials -->

        <?php
    endwhile; // End of the loop.
    ?>

<?php
get_footer();